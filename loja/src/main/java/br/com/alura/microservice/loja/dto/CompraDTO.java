package br.com.alura.microservice.loja.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CompraDTO {

	@JsonIgnore
	private Long CompraId;
	
	private List<ItemDaCompraDTO> itens;
	
	private EnderecoDTO endereco;
	
	public Long getCompraId() {
		return CompraId;
	}

	public void setCompraId(Long compraId) {
		CompraId = compraId;
	}

	public List<ItemDaCompraDTO> getItens() {
		return itens;
	}

	public void setItens(List<ItemDaCompraDTO> itens) {
		this.itens = itens;
	}

	public EnderecoDTO getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoDTO endereco) {
		this.endereco = endereco;
	}
	
	
}
